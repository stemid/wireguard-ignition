variable "wireguard_config" {
  type = object({
    name = string
    address = string
    destination = string
    gateway = string
    privatekey = string
    listenport = number
  })
}

variable "peers" {
  type = list(object({
    name = string
    address = string
    allowedips = string
    publickey = string
    privatekey = string
    presharedkey = string
    persistentkeepalive = number
  }))
}
