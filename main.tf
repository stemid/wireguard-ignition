# Only needed if you want to route traffic out to internet, or between
# client peers.
data "ignition_file" "sysctl" {
  path = "/etc/sysctl.d/90-ip_forward.conf"
  content {
    content = "net.ipv4.ip_forward=1"
  }
}

data "ignition_file" "modules" {
  path = "/etc/modules-load.d/wireguard.conf"
  content {
    content = "wireguard"
  }
}

# These two files only work with networkd, which is not layered in CoreOS.
data "ignition_file" "network" {
  path = "/etc/systemd/network/wg0.network"
  content {
    content = templatefile("${path.module}/templates/wireguard.network", {
      conf = var.wireguard_config
    })
  }
  mode = 384
}

data "ignition_file" "netdev" {
  path = "/etc/systemd/network/wg0.netdev"
  content {
    content = templatefile("${path.module}/templates/wireguard.netdev", {
      conf = var.wireguard_config
      peers = var.peers
    })
  }
  mode = 384
}

# This works with NetworkManager, but it's a hack that directly creates the
# file that nmcli conn import normally creates when you import a Wireguard conf.
data "ignition_file" "nmconnection" {
  path = "/etc/NetworkManager/system-connections/${var.wireguard_config.name}.nmconnection"
  content {
    content = templatefile("${path.module}/templates/wireguard.nmconnection", {
      conf = var.wireguard_config
      peers = var.peers
    })
  }
  mode = 384
}

# Final attempt, this is the only method I can get to work on CoreOS.
data "ignition_directory" "wg_config" {
  path = "/etc/wireguard"
}

data "ignition_file" "wg_config" {
  path = "/etc/wireguard/wg0.conf"
  content {
    content = templatefile("${path.module}/templates/wg0.conf", {
      conf = var.wireguard_config
      peers = var.peers
    })
  }
  mode = 384
}

data "ignition_systemd_unit" "wg" {
  name = "wg-quick@wg0.service"
  enabled = true
}

data "ignition_config" "config" {
  directories = [
    data.ignition_directory.wg_config.rendered
  ]

  files = [
    data.ignition_file.sysctl.rendered,
    data.ignition_file.modules.rendered,
    data.ignition_file.wg_config.rendered,
  ]

  systemd = [
    data.ignition_systemd_unit.wg.rendered,
  ]
}
